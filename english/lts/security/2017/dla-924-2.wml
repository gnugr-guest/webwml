<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA-924-1 introduced a regression in
Tomcat's APR protocol due to the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-5647">CVE-2017-5647</a> and prevented a
successful sendfile request.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u13.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-924-2.data"
# $Id: $

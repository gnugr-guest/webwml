<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various security vulnerabilities were discovered in sox, a command
line utility to convert audio formats, that may lead to a
denial-of-service (application crash / infinite loop) or memory
corruptions by processing a malformed input file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
14.4.0-3+deb7u2.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1197.data"
# $Id: $

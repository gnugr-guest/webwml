<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Applications using libtiff can trigger buffer overflows through
TIFFGetField() when processing TIFF images with unknown tags.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u2.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-692.data"
# $Id: $

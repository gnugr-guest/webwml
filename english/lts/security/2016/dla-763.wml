<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Saulius Lapinskas from Lithuanian State Social Insurance Fund Board
discovered that Squid3, a fully featured web proxy cache, does not
properly process responses to If-None-Modified HTTP conditional
requests, leading to client-specific Cookie data being leaked to other
clients. A remote attacker can take advantage of this flaw to discover
private and sensitive information about another client's browsing
session.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.1.20-2.2+deb7u7.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-763.data"
# $Id: $

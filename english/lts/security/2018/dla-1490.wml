<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities have been discovered in php5, a server-side,
HTML-embedded scripting language.  One (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14851">CVE-2018-14851</a>) results in a
potential denial of service (out-of-bounds read and application crash)
via a crafted JPEG file.  The other (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14883">CVE-2018-14883</a>) is an Integer
Overflow that leads to a heap-based buffer over-read.</p>

<p>Additionally, a previously introduced patch for <a href="https://security-tracker.debian.org/tracker/CVE-2017-7272">CVE-2017-7272</a> was found
to negatively affect existing PHP applications (#890266).  As a result
of the negative effects and the fact that the security team has marked
the CVE in question as "ignore," the patch has been dropped.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.37+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1490.data"
# $Id: $

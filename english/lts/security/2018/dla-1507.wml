<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jan Ingvoldstad discovered that libapache2-mod-perl2 allows attackers to
execute arbitrary Perl code by placing it in a user-owned .htaccess
file, because (contrary to the documentation) there is no configuration
option that permits Perl code for the administrator's control of HTTP
request processing without also permitting unprivileged users to run
Perl code in the context of the user account that runs Apache HTTP
Server processes.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.0.9~1624218-2+deb8u3.</p>

<p>We recommend that you upgrade your libapache2-mod-perl2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1507.data"
# $Id: $

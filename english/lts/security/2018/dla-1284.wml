<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Talosintelligence discovered a command injection vulnerability in the
gplotMakeOutput function of leptonlib. A specially crafted gplot
rootname argument can cause a command injection resulting in arbitrary
code execution. An attacker can provide a malicious path as input to an
application that passes attacker data to this function to trigger this
vulnerability.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.69-3.1+deb7u1.</p>

<p>We recommend that you upgrade your leptonlib packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1284.data"
# $Id: $

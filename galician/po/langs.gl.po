# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2012-12-06 22:24+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "Árabe"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "Armenio"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "Finés"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "Croata"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "Dinamarqués"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "Holandés"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "Inglés"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "Francés"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "Galego"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "Alemán"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "Italiano"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "Xaponés"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "Coreano"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "Español"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "Portugués"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "Portugués do Brasil"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "Chinés"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "Chinés (China)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "Chinés (Hong Kong)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "Chinés (Taiwan)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "Chinés (Tradicional)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "Chinés (Simplificado)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "Sueco"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "Polaco"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "Noruegués"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "Turco"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "Ruso"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "Checo"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "Esperanto"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "Húngaro"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "Romanés"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "Eslovaco"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "Grego"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "Catalán"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "Indonesio"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "Lituano"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "Esloveno"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "Búlgaro"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "Tamil"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "Africáner"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "Albanés"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "Asturiano"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "Amhárico"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "Azerí"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "Vasco"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "Bielorruso"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "Bengalí"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "Bosnio"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "Bretón"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "Córnico"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "Estoniano"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "Feroés"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "Gaélico escocés"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "Xeorxiano"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "Hebreo"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "Hindi"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "Islandés"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "Interlingua"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "Irlandés"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "Grenlandés"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "Canarés"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "Curdo"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "Letón"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "Macedonio"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "Malaio"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "Malaio"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "Maltés"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "Manxés"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "Maorí"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "Mongol"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "Noruegués bokm&aring;l"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "Noruegués nynorsk"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "Occitano (post 1500)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "Persa"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "Serbio"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "Esloveno"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "Taxico"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "Thai"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "Tonga"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "Twi"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "Ucraíno"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "Vietnamita"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "Galés"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "Xhosa"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "Iídiche"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "Zulu"

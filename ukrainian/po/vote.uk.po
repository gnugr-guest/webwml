# translation of templates.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2004, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"PO-Revision-Date: 2017-11-15 23:40+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: Ukrainian <ukrainian>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Дата"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Часова шкала"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Резюме"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Номінації"

#: ../../english/template/debian/votebar.wml:25
msgid "Debate"
msgstr "Обговорення"

#: ../../english/template/debian/votebar.wml:28
msgid "Platforms"
msgstr "Платформи"

#: ../../english/template/debian/votebar.wml:31
msgid "Proposer"
msgstr "Автор пропозиції"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposal A Proposer"
msgstr "Автор пропозиції А"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal B Proposer"
msgstr "Автор пропозиції Б"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal C Proposer"
msgstr "Автор пропозиції В"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal D Proposer"
msgstr "Автор пропозиції Г"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal E Proposer"
msgstr "Автор пропозиції Д"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal F Proposer"
msgstr "Автор пропозиції Е"

#: ../../english/template/debian/votebar.wml:52
msgid "Seconds"
msgstr "Підтримують пропозицію"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal A Seconds"
msgstr "Підтримують пропозицію А"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal B Seconds"
msgstr "Підтримують пропозицію Б"

#: ../../english/template/debian/votebar.wml:61
msgid "Proposal C Seconds"
msgstr "Підтримують пропозицію В"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal D Seconds"
msgstr "Підтримують пропозицію Г"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal E Seconds"
msgstr "Підтримують пропозицію Д"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal F Seconds"
msgstr "Підтримують пропозицію Е"

#: ../../english/template/debian/votebar.wml:73
msgid "Opposition"
msgstr "Опозиція"

#: ../../english/template/debian/votebar.wml:76
msgid "Text"
msgstr "Текст"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal A"
msgstr "Пропозиція А"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal B"
msgstr "Пропозиція Б"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal C"
msgstr "Пропозиція В"

#: ../../english/template/debian/votebar.wml:88
msgid "Proposal D"
msgstr "Пропозиція Г"

#: ../../english/template/debian/votebar.wml:91
msgid "Proposal E"
msgstr "Пропозиція Д"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal F"
msgstr "Пропозиція Е"

#: ../../english/template/debian/votebar.wml:97
msgid "Choices"
msgstr "Варіанти вибору"

#: ../../english/template/debian/votebar.wml:100
msgid "Amendment Proposer"
msgstr "Автор поправки"

#: ../../english/template/debian/votebar.wml:103
msgid "Amendment Seconds"
msgstr "Підтримують поправку"

#: ../../english/template/debian/votebar.wml:106
msgid "Amendment Text"
msgstr "Текст поправки"

#: ../../english/template/debian/votebar.wml:109
msgid "Amendment Proposer A"
msgstr "Автор поправки А"

#: ../../english/template/debian/votebar.wml:112
msgid "Amendment Seconds A"
msgstr "Підтримують поправку А"

#: ../../english/template/debian/votebar.wml:115
msgid "Amendment Text A"
msgstr "Текст поправки А"

#: ../../english/template/debian/votebar.wml:118
msgid "Amendment Proposer B"
msgstr "Автор поправки Б"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Seconds B"
msgstr "Підтримують поправку Б"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Text B"
msgstr "Текст поправки Б"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Proposer C"
msgstr "Автор поправки В"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Seconds C"
msgstr "Підтримують поправку В"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Text C"
msgstr "Текст поправки В"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendments"
msgstr "Поправки"

#: ../../english/template/debian/votebar.wml:139
msgid "Proceedings"
msgstr "Засідання"

#: ../../english/template/debian/votebar.wml:142
msgid "Majority Requirement"
msgstr "Умова більшості"

#: ../../english/template/debian/votebar.wml:145
msgid "Data and Statistics"
msgstr "Дані та статистика"

#: ../../english/template/debian/votebar.wml:148
msgid "Quorum"
msgstr "Кворум"

#: ../../english/template/debian/votebar.wml:151
msgid "Minimum Discussion"
msgstr "Мінімальне обговорення"

#: ../../english/template/debian/votebar.wml:154
msgid "Ballot"
msgstr "Бюлетень"

#: ../../english/template/debian/votebar.wml:157
msgid "Forum"
msgstr "Конференція"

#: ../../english/template/debian/votebar.wml:160
msgid "Outcome"
msgstr "Результат"

#: ../../english/template/debian/votebar.wml:164
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Очікує&nbsp;спонсорів"

#: ../../english/template/debian/votebar.wml:167
msgid "In&nbsp;Discussion"
msgstr "Розглядається"

#: ../../english/template/debian/votebar.wml:170
msgid "Voting&nbsp;Open"
msgstr "Голосування&nbsp;відкрите"

#: ../../english/template/debian/votebar.wml:173
msgid "Decided"
msgstr "Прийняте"

#: ../../english/template/debian/votebar.wml:176
msgid "Withdrawn"
msgstr "Відхилене"

#: ../../english/template/debian/votebar.wml:179
msgid "Other"
msgstr "Інше"

#: ../../english/template/debian/votebar.wml:183
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "На&nbsp;сторінку&nbsp;голосувань"

#: ../../english/template/debian/votebar.wml:186
msgid "How&nbsp;To"
msgstr "Як"

#: ../../english/template/debian/votebar.wml:189
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Внести&nbsp;пропозицію"

#: ../../english/template/debian/votebar.wml:192
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Внести&nbsp;поправку"

#: ../../english/template/debian/votebar.wml:195
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Підтримати&nbsp;пропозицію"

#: ../../english/template/debian/votebar.wml:198
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Взнати&nbsp;результат"

#: ../../english/template/debian/votebar.wml:201
msgid "Vote"
msgstr "Проголосувати"

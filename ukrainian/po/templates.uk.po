# translation of templates.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2004, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"PO-Revision-Date: 2017-11-18 13:50+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: українська <Ukrainian <ukrainian>>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Веб-сайт Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Пошук веб-сайту Debian."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:46
msgid "Debian website search"
msgstr "Пошук по веб-сайту Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Так"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Ні"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Проект Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian - це операційна система та дистрибутив вільного програмного "
"забезпечення. Він підтримується та оновлюється працею великої кількості "
"людей, що добровільно присвячують йому свій час та зусилля."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr ""
"debian, дебіан, GNU, linux, лінукс, лінакс, unix, юнікс, вільне програмне "
"забезпечення, open source, free, вільне, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr ""
"Повернутися на <a href=\"m4_HOME/\">головну сторінку проекту Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Домівка"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Пропустити меню"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Про&nbsp;Debian"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Про Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Як з нами зв'язатися"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
#| msgid "Release Info"
msgid "Legal Info"
msgstr "Інформація про випуски"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Пожертви"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Події"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Новини"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Підтримка"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Чисті суміші"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Куток розробника"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Документація"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Інформація про безпеку"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Пошук"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "ні"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Перейти"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "всюди"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Карта сайту"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Різне"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Отримання Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Блог Debian"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Надсилайте, будь ласка, всі коментарі, критику та побажання, пов'язані з "
"цими сторінками на наш <a href=\"mailto:debian-doc@lists.debian.org\">список "
"розсилки</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "непотрібно"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "недоступно"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/A"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "у випуску 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "у випуску 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "у випуску 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "у випуску 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "у випуску 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:107
#, fuzzy
#| msgid ""
#| "To report a problem with the web site, e-mail our publicly archived "
#| "mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-"
#| "www@lists.debian.org</a>.  For other contact information, see the Debian "
#| "<a href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a "
#| "href=\"m4_HOME/devel/website/using_cvs\">available</a>."
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Щоб повідомити про проблему із веб-сайтом, надішліть лист на нашу публічну "
"адресу <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a>. Додаткову контактну інформацію дивіться на сторінці <a href="
"\"m4_HOME/contact\">контактної інформації</a>. Веб-сайт із джерельним кодом "
"доступний <a href=\"m4_HOME/devel/website/using_cvs\">тут</a>."

#: ../../english/template/debian/footer.wml:110
msgid "Last Modified"
msgstr "Остання модифікація"

#: ../../english/template/debian/footer.wml:113
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:116
msgid "Copyright"
msgstr "Авторські права"

#: ../../english/template/debian/footer.wml:119
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> та інші;"

#: ../../english/template/debian/footer.wml:122
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Дивіться <a href=\"m4_HOME/license\" rel=\"copyright\">умови ліцензії</a>"

#: ../../english/template/debian/footer.wml:125
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian є зареєстрованою <a href=\"m4_HOME/trademark\">торговою маркою</a> "
"компанії Software in the Public Interest, Inc. (Програмне забезпечення в "
"інтересах суспільства)."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Ця сторінка також доступна наступними мовами:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Як встановити <a href=m4_HOME/intro/cn>мову документа за замовчанням</a>."

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Міжнародний Debian"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Партнери"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Щотижневі новини Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Щотижневі новини"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Новини роекту Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Новини проекту"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Інформація про випуски"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Пакунки Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Завантаження"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;на&nbsp;КД"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Книги Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Debian Вікі"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Архіви списків розсилки"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Списки розсилки"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Суспільний договір"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Кодекс поведінки"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - Універсальна операційна система"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Мапа сайту Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "База даних розробників"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "ЧаПи Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Посібник по політиці Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Довідник розробника"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Посібник нового розробника"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Помилки, критичні для випуску"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Звіти Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Архіви списків розсилки для користувачів"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Архіви списків розсилки для розробників"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Архіви списків розсилки по інтернаціоналізації та локалізації"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Архіви списків розсилки по перенесенню на різні архітектури"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Архіви списків розсилки системи відслідковування помилок"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Архіви інших списків розсилки"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Вільне програмне забезпечення"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Розробка"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Як допомогти Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Звіти про помилки"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Перенесення&nbsp;на&nbsp;інші&nbsp;архітектури"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Посібник по встановленню"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Постачальники КД"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "ISO образи КД/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Встановлення через мережу"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Передвстановлення"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Проект Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth&nbsp;&mdash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Контроль якості"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Система відслідковування пакунків"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Огляд пакунків для розробників Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Домівка Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Немає нічого за цей рік."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "запропоновані"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "на обговоренні"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "голосування відкрите"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "завершені"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "скасовані"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Майбутні події"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Минулі події"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(нова версія)"

#: ../../english/template/debian/recent_list.wml:303
msgid "Report"
msgstr "Доповідь"

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s для %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Зауваження:</em> <a href=\"$link\">оригінальний документ</a> новіший за "
"цей переклад."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Увага! Цей переклад дуже застарів, дивіться, будь ласка, <a href=\"$link"
"\">оригінал</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""
"<em>Зауваження:</em> Оригінальний документ цього перекладу більше не існує."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:7
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Повернутися на <a href=\"./\">сторінку користувачів Debian</a>."

#~ msgid "More information"
#~ msgstr "Додаткова інформація"

#~ msgid "Upcoming Attractions"
#~ msgstr "Майбутні події"

#~ msgid "link may no longer be valid"
#~ msgstr "посилання може бути більше не діяти"

#~ msgid "When"
#~ msgstr "Коли"

#~ msgid "Where"
#~ msgstr "Де"

#~ msgid "More Info"
#~ msgstr "Додаткова інформація"

#~ msgid "Debian Involvement"
#~ msgstr "Участь Debian"

#~ msgid "Main Coordinator"
#~ msgstr "Головний координатор"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Координатор</th><th>проекту</th>"

#~ msgid "Related Links"
#~ msgstr "Пов'язані посилання"

#~ msgid "Latest News"
#~ msgstr "Останні новини"

#~ msgid "Download calendar entry"
#~ msgstr "Завантажити календарний запис"

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Повернутися до: інших <a href=\"./\">новин Debian</a> || <a href="
#~ "\"m4_HOME/\">головну сторінку проекту Debian</a>."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (мертве посилання)"

#, fuzzy
#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"http://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Щоб кожен тиждень отримувати новини поштою, <a href=\"http://lists.debian."
#~ "org/debian-news/\">підпишіться на список розсилки debian-news</a>."

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr "Також наявні <a href=\"../../\">минулі випуски</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Щотижневі новини Debian випускає <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Щотижневі новини Debian випускають <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />This issue of Debian Weekly News was edited by "
#~| "<a href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Цей випуск щотижневих новин випустив <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~| "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Цей випуск щотижневих новин випустили <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />Переклав %s."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />Переклали %s."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />Переклала %s."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />Переклали %s."

#~ msgid "List of Speakers"
#~ msgstr "Список доповідачів"

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Повернутися на <a href=\"./\">сторінку доповідачів Debian</a>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "Щоб кожен тиждень отримувати новини поштою, <a href=\"http://lists.debian."
#~ "org/debian-news/\">підпишіться на список розсилки debian-news</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Щотижневі новини Debian випускає <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Щотижневі новини Debian випускають <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Цей випуск щотижневих новин випустив <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Цей випуск щотижневих новин випустили <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "No requests for adoption"
#~ msgstr "Запитів про „усиновлення“ не поступали"

#~ msgid "No orphaned packages"
#~ msgstr "Немає „осиротілих“ пакунків"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "Немає пакунків, що очікують на „усиновлення“"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "Немає пакунків, по яким ведеться робота"

#~ msgid "No Requested packages"
#~ msgstr "Немає запитів на створення пакунків"

#~ msgid "No help requested"
#~ msgstr "Немає запитів про допомогу"

#~ msgid "in adoption since today."
#~ msgstr "„усиновлення“ почалося сьогодні."

#~ msgid "in adoption since yesterday."
#~ msgstr "„усиновлення“ почалося вчора."

#~ msgid "%s days in adoption."
#~ msgstr "„усиновлення“ почалося %s днів тому."

#~ msgid "in preparation since today."
#~ msgstr "робота почалась сьогодні."

#~ msgid "in preparation since yesterday."
#~ msgstr "робота почалась вчора."

#~ msgid "%s days in preparation."
#~ msgstr "робота почалась %s днів тому."

#~ msgid "requested today."
#~ msgstr "запит отриманий сьогодні."

#~ msgid "requested yesterday."
#~ msgstr "запит отриманий вчора."

#~ msgid "requested %s days ago."
#~ msgstr "запит отриманий %s днів тому."

#~ msgid "package info"
#~ msgstr "інформація про пакунок"

#~ msgid "License Information"
#~ msgstr "Інформація про ліцензії"

#~ msgid "DLS Index"
#~ msgstr "Індекс DLS"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DFSG FAQ"
#~ msgstr "FAQ по DFSG"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Архів Debian-Legal"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s&nbsp;&mdash; %s: %s"

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s&nbsp;&mdash; %s, версія %s: %s"

#~ msgid "Date published"
#~ msgstr "Дата публікації"

#~ msgid "License"
#~ msgstr "Ліцензія"

#~ msgid "Version"
#~ msgstr "Версія"

#~ msgid "Summary"
#~ msgstr "Резюме"

#~ msgid "Justification"
#~ msgstr "Обґрунтування"

#~ msgid "Discussion"
#~ msgstr "Обговорення"

#~ msgid "Original Summary"
#~ msgstr "Початкове резюме"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "Автор початкового резюме: <summary-author/>. Резюме може бути знайдене в "
#~ "<a href=\"<summary-url/>\">списку розсилки</a>."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Підготував це резюме <summary-author/>."

#~ msgid "License text (translated)"
#~ msgstr "Текст ліцензії (перекладений)"

#~ msgid "License text"
#~ msgstr "Текст ліцензії"

#~ msgid "free"
#~ msgstr "вільна"

#~ msgid "non-free"
#~ msgstr "невільна"

#~ msgid "not redistributable"
#~ msgstr "з обмеженнями на розповсюдження"

#~ msgid "Free"
#~ msgstr "Вільні"

#~ msgid "Non-Free"
#~ msgstr "Невільні"

#~ msgid "Not Redistributable"
#~ msgstr "З обмеженнями на розповсюдження"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Дивіться сторінку <a href=\"./\">інформації про ліцензії</a> для огляду "
#~ "резюме Debian по ліцензіям (Debian License Summaries&nbsp;&mdash; DLS)."

#~ msgid "Vote"
#~ msgstr "Проголосувати"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Взнати&nbsp;результат"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Підтримати&nbsp;пропозицію"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Внести&nbsp;поправку"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Внести&nbsp;пропозицію"

#~ msgid "How&nbsp;To"
#~ msgstr "Як"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "На&nbsp;сторінку&nbsp;голосувань"

#~ msgid "Other"
#~ msgstr "Інше"

#~ msgid "Withdrawn"
#~ msgstr "Відхилене"

#~ msgid "Decided"
#~ msgstr "Прийняте"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Голосування&nbsp;відкрите"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Розглядається"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Очікує&nbsp;спонсорів"

#~ msgid "Ballot"
#~ msgstr "Бюлетень"

#~ msgid "Text"
#~ msgstr "Текст"

#~ msgid "Platforms"
#~ msgstr "Платформи"

#~ msgid "Date"
#~ msgstr "Дата"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "Англомовний <a href=\"/MailingLists/disclaimer\">список розсилки</a> "
#~ "присвячений CD/DVD:"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />різне"

#~ msgid "net_install"
#~ msgstr "встановлення через мережу"

#~ msgid "buy"
#~ msgstr "купити"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "jigdo"
#~ msgstr "jidgo"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />FAQ"

#~ msgid "debian_on_cd"
#~ msgstr "Debian на CD"

#~ msgid "Debian CD team"
#~ msgstr "Команда CD Debian"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Завантажити через Torrent"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Rsync-дзеркала"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Відображення"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Різне"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Завантажити"

#~ msgid "Network Install"
#~ msgstr "Встановлення через мережу"

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Купити CD або DVD"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Завантажити через HTTP/FTP"

#~ msgid "Download with Jigdo"
#~ msgstr "Завантажити за допомогою Jigdo"

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Повернутися на <a href=\"./\">сторінку консультантів Debian</a>."

#~ msgid "List of Consultants"
#~ msgstr "Список консультантів"

#~ msgid "Rating:"
#~ msgstr "Рейтинг:"

#~ msgid "Nobody"
#~ msgstr "Ніхто"

#~ msgid "Taken by:"
#~ msgstr "Відповідальний:"

#~ msgid "More information:"
#~ msgstr "Додаткова інформація:"

#~ msgid "Select a server near you"
#~ msgstr "Виберіть найближчий сервер"

#~ msgid "Visit the site sponsor"
#~ msgstr "Відвідайте нашого спонсора"

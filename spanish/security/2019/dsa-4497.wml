#use wml::debian::translation-check translation="c13faedbded510feb7e5f7ea5a004d0206db58d4"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas
de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8553">CVE-2015-8553</a>

    <p>Jan Beulich descubrió que el <a href="https://security-tracker.debian.org/tracker/CVE-2015-2150">CVE-2015-2150</a> no se abordó
    completamente. Si se traslada una función física PCI a un
    huésped Xen, el huésped puede acceder a su memoria y regiones
    de E/S antes de que se habilite la decodificación de esas regiones. Esto podría
    dar lugar a denegación de servicio (NMI inesperado) en el anfitrión.</p>

    <p>La corrección para esto es incompatible con versiones de qemu anteriores a la 2.5.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18509">CVE-2017-18509</a>

    <p>Denis Andzakovic informó de la falta de una comprobación de tipo en la implementación
    del enrutamiento multidifusión con IPv4. Un usuario con capacidad CAP_NET_ADMIN (en
    cualquier espacio de nombres de usuario) podría usar esto para provocar denegación de servicio (corrupción
    de memoria o caída) o, posiblemente, para elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

    <p>ADLab, de VenusTech, descubrió que el núcleo registraba las direcciones
    virtuales asignadas a datos específicos de cada CPU, lo que podría hacer más fácil
    explotar otras vulnerabilidades.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang informó de una condición de carrera en libsas, el subsistema
    del núcleo para soporte de dispositivos SCSI en serie (SAS, por sus siglas en inglés), que
    podría dar lugar a un «uso tras liberar». No está claro cómo podría
    explotarse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20856">CVE-2018-20856</a>

    <p>Xiao Jin informó de una potencial «doble liberación» en el subsistema orientado a bloques,
    en caso de que se produzca un error durante la inicialización del planificador de E/S para un
    dispositivo orientado a bloques. No está claro cómo podría explotarse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>Se descubrió que la mayoría de los procesadores x86 podrían saltarse,
    de forma especulativa, una instrucción SWAPGS condicional utilizada al entrar al
    núcleo desde modo usuario, o que podrían ejecutarla, de forma especulativa, cuando
    deberían saltársela. Este es un subtipo de Spectre variante 1
    que podría permitir que usuarios locales obtengan información sensible
    del núcleo o de otros procesos. Se ha mitigado utilizando
    barreras de memoria para limitar la ejecución especulativa. A los sistemas con
    núcleo i386 no les afecta ya que el núcleo no utiliza SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

    <p>Se encontró que la implementación de vfio no limitaba el número de
    correspondencias («mappings») DMA con la memoria del dispositivo. Un usuario local propietario
    de un dispositivo vfio podría usar esto para provocar denegación de servicio
    (memoria agotada).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3900">CVE-2019-3900</a>

    <p>Se descubrió que los controladores vhost no gestionaban correctamente la
    cantidad de trabajo realizado para atender peticiones de las VM huéspedes. Un
    huésped malicioso podría usar esto para provocar denegación de servicio
    (uso ilimitado de CPU) en el anfitrión.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>La herramienta syzkaller encontró una potencial desreferencia nula en varios
    controladores para adaptadores de Bluetooth en serie (conectados a UART). Un usuario local con
    acceso a un dispositivo pty o a otro dispositivo tty adecuado podría aprovecharse de esto
    para provocar una denegación de servicio (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein y Benny Pinkas descubrieron que la generación de ID
    de paquetes IP utilizaba una función hash débil: <q>jhash</q>. Esto podría permitir
    el seguimiento de ordenadores individuales al comunicarse con distintos
    servidores remotos y desde redes también distintas. En su lugar,
    ahora se utiliza la función <q>siphash</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

    <p>Amit Klein y Benny Pinkas descubrieron que la generación de ID
    de paquetes IP utilizaba una función hash débil que incorporaba una dirección
    virtual del núcleo. Esta función hash ya no se utiliza para los ID IP,
    aunque todavía se usa con otros fines en la pila de protocolos de red.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>Se descubrió que el controlador gtco para tabletas de entrada USB podría
    desbordar la pila con datos constantes durante el análisis sintáctico del descriptor del
    dispositivo. Un usuario presente físicamente con un dispositivo
    USB construido de una manera especial podría usar esto para provocar denegación de servicio
    (BUG/oops) o, posiblemente, para elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey informó de que en sistemas PowerPC (ppc64el) sin
    memoria transaccional (TM, por sus siglas en inglés), el núcleo, de todas formas, intentaría
    restaurar el estado de TM pasado a la llamada al sistema sigreturn(). Un usuario
    local podría usar esto para provocar denegación de servicio (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>La herramienta syzkaller encontró que faltaba una comprobación de límites en el controlador de
    disquete. Un usuario local con acceso a una disquetera con un
    disquete en su interior podría usar esto para leer memoria del núcleo fuera de los límites del
    área de E/S y obtener, posiblemente, información sensible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>La herramienta syzkaller encontró una potencial división por cero en el
    controlador de disquete. Un usuario local con acceso a una
    disquetera podría usar esto para provocar denegación de servicio (oops).</p></li>

<li>(Todavía no asignado ID de CVE)

    <p>Denis Andzakovic informó de un posible «uso tras liberar» en la
    implementación de sockets TCP. Un usuario local podría usar esto para provocar
    denegación de servicio (corrupción de memoria o caída) o, posiblemente,
    elevación de privilegios.</p></li>

<li>(Todavía no asignado ID de CVE)

    <p>El subsistema conntrack de netfilter utilizaba direcciones del núcleo como
    ID visibles por el usuario, lo que podría hacer más fácil explotar otras
    vulnerabilidades de seguridad.</p></li>

<li>XSA-300

    <p>Julien Grall informó de que Linux no limita la cantidad de memoria
    que puede intentar utilizar un dominio ni la cantidad de
    memoria de mapeo externo o de mapeo de permisos («foreign / grant map») que puede consumir un huésped individual,
    dando lugar a condiciones de denegación de servicio (para el anfitrión o para los huéspedes).</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 4.9.168-1+deb9u5.</p>

<p>Para la distribución «estable» (buster), estos problemas se corrigieron en su mayor parte
en la versión 4.19.37-5+deb10u2 o anteriores.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4497.data"

#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a>

<p>(1) La source d’authentification Htpasswd dans le module authcrypt et (2)
la classe SimpleSAML_Session dans SimpleSAMLphp 1.14.11 et précédents permettent
à des attaquants distants de mener des attaques temporelles par canal auxiliaire
en exploitant l’utilisation de l’opérateur de comparaison standard pour comparer
le document secret avec l’entrée de l’utilisateur.</p>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a>
concernait un correctif inapproprié de
<a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a>
dans la correction initiale publiée par l’amont. Nous avons utilisé le correctif
approprié.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.13.1-2+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets simplesamlphp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1408.data"
# $Id: $

#use wml::debian::translation-check translation="28571f1f95b81eb7b756c120eb0962f456fdea16" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Imre Rad a découvert plusieurs vulnérabilités dans GNU patch, menant à
l'injection de commande de l'interpréteur ou à la fuite du répertoire de
travail et à l'accès à des fichiers et à leur écrasement lors du traitement
de fichiers patch contrefaits pour l'occasion.</p>

<p>Cette mise à jour inclut une correction de bogue pour une régression
introduite dans le correctif pour traiter
<a href="https://security-tracker.debian.org/tracker/CVE-2018-1000156">CVE-2018-1000156</a>
lors de l'application d'un fichier patch de type ed (nº 933140).</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 2.7.5-1+deb9u2.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.7.6-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets patch.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de patch, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/patch">\
https://security-tracker.debian.org/tracker/patch</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4489.data"
# $Id: $

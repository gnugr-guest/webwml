#use wml::debian::translation-check translation="dbf4ba8e9bfb35169a40a69893c185951be6f765" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Raphaël Arrouas et Jean Lejeune ont découvert une vulnérabilité de
contournement de contrôle d'accès dans mod_jk, le connecteur d'Apache pour
le moteur de servlet Java Tomcat. La vulnérabilité est traitée en mettant à
niveau mod_jk vers la version amont 1.2.46 qui comprend des modifications
supplémentaires.</p>

<ul>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.42_and_1.2.43">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.42_and_1.2.43</a></li>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.43_and_1.2.44">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.43_and_1.2.44</a></li>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.44_and_1.2.45">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.44_and_1.2.45</a></li>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.45_and_1.2.46">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.45_and_1.2.46</a></li>
</ul>

<p>Pour la distribution stable (Stretch), ce problème a été corrigé dans la
version 1:1.2.46-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache-mod-jk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapache-mod-jk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache-mod-jk">\
https://security-tracker.debian.org/tracker/libapache-mod-jk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4357.data"
# $Id: $

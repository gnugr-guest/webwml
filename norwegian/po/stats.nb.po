msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-04-17 09:11+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistikk for oversettelsen av Debian sitt nettsted"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Det er %d sider å oversette."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Det er %d byte å oversette."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Det er %d strenger å oversette."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Feil oversettelsesversjon"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Denne oversettelsen for utdatert"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Originalen er nyere enn denne oversettelsen"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Originalen fins ikke lenger"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "antall treff fins ikke"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "treff"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr ""

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Oversettelsessammendrag for"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Ikke oversatt"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Utdatert"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Oversatt"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Oppdatert"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "filer"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "byte"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Merk: Lista med sider er sortert etter popularitet. Hold musepekeren over "
"sidenavnet for å se antall treff."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Utdaterte oversettelser"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Fil"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr ""

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr ""

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Logg"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Oversettelse"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Vedlikeholder"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Oversetter"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Dato"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Generelle sider som ikke er oversatt"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Ikke-oversatte generelle sider"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nyhetsoppføringer som ikke er oversatt"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Ikke-oversatte nyhetsoppføringer"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Konsulent/brukersider som ikke er oversatt"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Ikke-oversatte konsulent/brukersider"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Internasjonale sider som ikke er oversatt"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Ikke-oversatte internasjonale sider"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Oversatte sider (oppdatert)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Oversatte maler (PO-filer)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "PO-oversettelsesstatistikk"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Ikke oversatt"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Totalt"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Totalt:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Oversatte nettsider"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Oversettelsesstatistikk etter sideantall"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Språk"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Oversettelser"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Oversatte nettsider (etter størrelse)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Oversettelsesstatistikk etter sidestørrelse"

#~ msgid "Created with"
#~ msgstr "Opprettet med"

#~ msgid "Origin"
#~ msgstr "Kilde"

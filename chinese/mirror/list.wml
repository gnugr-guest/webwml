#use wml::debian::template title="Debian 全球鏡像站" BARETITLE=true
#use wml::debian::translation-check translation="ef7400a6ea2c98aed253709dd68a428201e21d88" maintainer="Kanru Chen"

<p>Debian 正透過數百個伺服器（<em>鏡像</em>）在網路上散佈。使用離您
較近的伺服器將可能提高下載速度，並且也減輕中央伺服器與全球網路的負擔。</p>

<p>Debian 鏡像可以分成主要與次要站。定義如下：</p>

<p class="centerblock">
  <strong>主要鏡像站</strong>有足夠的頻寬，
  并直接与 Debian 内部的 syncproxy 网络保持同步。
  一些主要镜像站具有类似
  ftp.&lt;國家&gt;.debian.org 的别名，方便用户记忆。
  它们通常包含所有硬件架构。
</p>

<p class="centerblock">
  <strong>次要鏡像站</strong>可能会限制它們[CN:鏡像:][HKTW:映射:]的
  内容（因为空间有限）。
  身為一個次要鏡像站並不代表它會比其他主要鏡像站慢或是較少更新。事实上，
  您几乎总是应当优先选择包含您所使用的硬件架构且离您更近（所以更快）
  的次要镜像站，而非离您很远的主要镜像站。
</p>

<p>不論是主要鏡像站或是次要鏡像站，使用離您最近的站台以取得最快的下載速度。
名為
<a href="https://packages.debian.org/stable/net/netselect">
<em>netselect</em></a> 的程式可以用來決定哪個站台有最小的延遲；使用下載程式
如
<a href="https://packages.debian.org/stable/web/wget">
<em>wget</em></a> 或是
<a href="https://packages.debian.org/stable/net/rsync">
<em>rsync</em></a> 來決定哪些站台有最大的輸出頻寬。
注意地理上的接近可能不是決定哪個站台最適合您的最重要因素。</p>

<p>
如果您的机器经常移动，使用基于
全球 <abbr title="内容分发网络">CDN</abbr> 的“镜像”效果可能更好。
Debian 项目将域名 <code>deb.debian.org</code> 用作此种目的，您可以
在 apt 的 sources.list 中使用它 &mdash; 访问\
<a href="http://deb.debian.org/">此服务的网站</a>以获得详细说明。

<p>官方镜像站列表總是可以在：<url "https://www.debian.org/mirror/list"> 取得。
其他關於 Debian 鏡像的訊息可以在此獲得：
<url "https://www.debian.org/mirror/">。
</p>

<h2 class="center">主要 Debian 镜像站</h2>

<table border="0" class="center">
<tr>
  <th>國家/地區</th>
  <th>站台</th>
  <th>硬體架構</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">次要 Debian 镜像站</h2>

<table border="0" class="center">
<tr>
  <th>主機名稱</th>
  <th>HTTP</th>
  <th>硬體架構</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"

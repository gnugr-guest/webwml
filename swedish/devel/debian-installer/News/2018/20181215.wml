#use wml::debian::translation-check translation="9a091cbc67457151ac60a49ad92557120fe3f69f"
<define-tag pagetitle>Alpha 4 av utgåvan av Debian Buster-installeraren</define-tag>
<define-tag release_date>2018-12-15</define-tag>
#use wml::debian::news

<p>
Debian Installerar-<a
href="https://wiki.debian.org/DebianInstaller/Team">gruppen</a> tillkännager
stolt den fjärde alfa-utgåvan av installeraren för Debian 10
<q>Buster</q>.
</p>


<h2>Förord</h2>

<p>Cyril Brulebois skulle vilja börja med att tacka Christian Perrier,
som spenderade många år med att arbeta på Debianinstalleraren, speciellt
på internationalisering (i18n) och lokalisering (l10n). Man kan komma
ihåg grafer och bloggposter med statistik på Planet Debian;
att hålla reda på dessa siffror kan verka vara ett helt matematiskt
ämne, men att ha uppdaterade översättningar är en viktig del i att ha
en Debianinstallerare som är tillgänglig för de flesta användare.</p>

<p>Tack så mycket, Christian!</p>

<h2>Förbättringar i denna utgåva</h2>

<ul>
  <li>choose-mirror:
    <ul>
      <li>Sätt deb.debian.org som standard-HTTP-spegling (<a href="https://bugs.debian.org/797340">#797340</a>).</li>
    </ul>
  </li>
  <li>debian-archive-keyring-udeb:
    <ul>
      <li>Ta bort wheezy-nycklar (<a href="https://bugs.debian.org/901320">#901320</a>).</li>
      <li>Börja skeppa separata nyckelringar för varje utgåva
        (<a href="https://bugs.debian.org/861695">#861695</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Knuffa upp Linuxkärn-ABIn från 4.16.0-2 till 4.18.0-3.</li>
      <li>Gör netboot.tar.gz's arkivstruktur för armhf identisk med
        alla andra arkitekturer (<a href="https://bugs.debian.org/902020">#902020</a>).</li>
      <li>Ersätt ttf-freefont-udeb med fonts-freefont-udeb.</li>
      <li>Behåll grub-upplösning i EFI boot, för att undvika små typsnitt
        (<a href="https://bugs.debian.org/910227">#910227</a>).</li>
      <li>Fixa icke-fungerande F10-knappsfunktion i hjälpsidor i bootskärmen.</li>
    </ul>
  </li>
  <li>debootstrap:
    <ul>
      <li>Aktiverad sammanslagen-/usr som standard (<a href="https://bugs.debian.org/839046">#839046</a>), men inaktivera den när
	    en buildd chroot förbereds (<a href="https://bugs.debian.org/914208">#914208</a>).</li>
      <li>Många andra förbättringar/rättningar, se ändringsloggen för detaljer.</li>
    </ul>
  </li>
  <li>fonts-thai-tlwg-udeb:
    <ul>
      <li>Skeppa OTF-typsnitt istället för TTF.</li>
    </ul>
  </li>
  <li>partman-auto-lvm:
    <ul>
      <li>Lägg till möjlighet att begränsa utrymme som används i LVM VG (<a href="https://bugs.debian.org/515607">#515607</a>,
        <a href="https://bugs.debian.org/904184">#904184</a>).</li>
    </ul>
  </li>
  <li>partman-crypto:
    <ul>
      <li>Sätt discard-alternativet på LUKS-behållare (<a href="https://bugs.debian.org/902912">#902912</a>).</li>
    </ul>
  </li>
  <li>pkgsel:
    <ul>
      <li>Installera inte längre unattended-upgrades som standard
		<a href="https://lists.debian.org/debian-boot/2018/05/msg00250.html"> som
		  efterfrågats av Debians säkerhetsgrupp</a>.</li>
      <li>Installerad nya beroenden när safe-upgrade (standard) är
        valt (<a href="https://bugs.debian.org/908711">#908711</a>). Specifikt så säkerställer detta att
		det senaste linux-image-paketet installeras.</li>
      <li>Tillåt update-initramfs att köra normalt under paketuppgradering och installation
	   (<a href="https://bugs.debian.org/912073">#912073</a>).</li>
    </ul>
  </li>
  <li>preseed:
    <ul>
      <li>Markera 'Checksum error'-strängar som översättningsbara.</li>
    </ul>
  </li>
</ul>


<h2>Förändringar i hårdvarustöd</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>[armel] Inaktivera OpenRD-mål, som inte längre finns i
        u-boot.</li>
      <li>[armhf] Ta bort Firefly-RK3288-avbildning, eftersom u-boot misslyckas att boota
        på grund av <a href="https://bugs.debian.org/898520">#898520</a>.</li>
      <li>[armhf] Lägg till avbildning för Sinovoip_BPI_M3.</li>
      <li>[arm64] Lägg till u-boot-avbildning för pinebook.</li>
      <li>[mips*el/loongson-3] Lägg till input-moduler till netbootavbildning
        (<a href="https://bugs.debian.org/911664">#911664</a>).</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Lägg till maskin-db-post för Helios-4 NAS system
        (<a href="https://bugs.debian.org/914016">#914016</a>).</li>
      <li>Lägg till maskin-db-post för Rockchip RK3288 Tinker Board
        (<a href="https://bugs.debian.org/895934">#895934</a>).</li>
      <li>Uppdatera Firefly-RK3399 Board (<a href="https://bugs.debian.org/899091">#899091</a>).</li>
      <li>Lägg till Rockchip RK3399 Evaluation Board (<a href="https://bugs.debian.org/899090">#899090</a>).</li>
      <li>Uppdatera post för Marvell 8040 MACCHIATOBin (<a href="https://bugs.debian.org/899092">#899092</a>).</li>
      <li>Uppdatera Pine64+ (<a href="https://bugs.debian.org/899093">#899093</a>).</li>
      <li>Uppdatera Raspberry Pi 3 Model B (<a href="https://bugs.debian.org/899096">#899096</a>).</li>
      <li>Lägg till post för Raspberry PI 3 B+ (<a href="https://bugs.debian.org/905002">#905002</a>).</li>
      <li>Clearfog Pro: korrigera DTB-namn (<a href="https://bugs.debian.org/902432">#902432</a>).</li>
      <li>Lägg till saknade poster för HummingBoard-varianter (<a href="https://bugs.debian.org/905962">#905962</a>).</li>
      <li>Lägg till poster för ytterligare Cubox-i modeller: SolidRun Cubox-i
        Dual/Quad (1.5som), och SolidRun Cubox-i Dual/Quad
        (1.5som+emmc).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>udeb: Lägg till virtio_console till virtio-modules (<a href="https://bugs.debian.org/903122">#903122</a>).</li>
    </ul>
  </li>
  <li>parted:
    <ul>
      <li>Rätta läsning av NVMe modellnamn från sysfs (<a href="https://bugs.debian.org/911273">#911273</a>).</li>
    </ul>
  </li>
  <li>u-boot:
    <ul>
      <li>[armhf] u-boot-rockchip: Överge firefly-rk3288-målet
        (<a href="https://bugs.debian.org/898520">#898520</a>).</li>
      <li>[arm64] u-boot-sunxi: Aktivera a64-olinuxino-målet
        (<a href="https://bugs.debian.org/881564">#881564</a>).</li>
      <li>[arm64] u-boot-sunxi: Lägg till pinebook-mål.</li>
      <li>[armel] Överge openrd-mål (stöds inte länge uppströms)).</li>
      <li>[armhf] u-boot-sunxi: Aktivera Sinovoip Banana Pi M3
        (<a href="https://bugs.debian.org/905922">#905922</a>).</li>
      <li>u-boot-imx: Ta bort mx6cuboxi4x4-målet, eftersom ram numer
		detekteras ordentligt med mx6cuboxi.</li>
      <li>[armhf] u-boot-sunxi: Aktivera A20-OLinuXino-Lime2-eMMC
        (<a href="https://bugs.debian.org/901666">#901666</a>).</li>
    </ul>
  </li>
</ul>


<h2>Lokaliseringsstatus</h2>

<ul>
  <li>76 språk stöds i denna utgåva.<br />
    Notera: Engelska räknades inte i tidigare tillkännagivelser.
  </li>
  <li>Fullständig översättning för 25 av dem.<br />
	Bra jobbat av Holger Wansing, våra nya översättningskoordinerare, och
	alla inblandade översättare!
  </li>
</ul>


<h2>Kända problem i denna utgåva</h2>

<p>
Se <a href="$(DEVEL)/debian-installer/errata">errata</a> för detaljer
och en fullständig lista på kända problem.
</p>


<h2>Återkoppling för denna utgåva</h2>

<p>
Vi behöver din hjälp för att hitta fel och ytterligare förbättra installeraren,
så vänligen testa den. Installerar-CDs, annan media och allt annat du kommer
att behöva finns tillgängligt på vår <a href="$(DEVEL)/debian-installer">webbsida</a>.
</p>


<h2>Tack</h2>

<p>
Debian-installerargruppen tackar alla som har bidragit till denna utgåva.
</p>

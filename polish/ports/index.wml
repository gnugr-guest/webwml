#use wml::debian::template title="Adaptacje (porty)"
#use wml::debian::toc
#use wml::debian::translation-check translation="5a1b0ae1bf020f2ce5587fb743650f3991922238"
#include "$(ENGLISHDIR)/releases/info"

<toc-display/>

<toc-add-entry name="intro">Wstęp</toc-add-entry>
<p>
 Większość z was wie zapewne, że <a href="https://www.kernel.org/">Linux</a>
 jest jedynie jądrem, a przez długi czas działał tylko na komputerach z
 procesorami Intela z rodziny x86 począwszy od i386 w górę.
</p>
<p>
 Od dłuższego czasu ograniczenie to przestało istnieć. Jądro Linux zostało
 przeniesione na dużą i wciąż rosnącą liczbę architektur. Idąc tym tropem
 zaadoptowaliśmy do nich również Debiana. Działanie to, najkrócej rzecz
 ujmując, zaczyna się od żmudnej pracy (dopóki nie doprowadzimy libc i
 dynamicznego konsolidatora do stanu użyteczności), po której następuje
 względnie rutynowy, acz długi okres, kiedy to staramy się skompilować dla
 nowej architektury wszystkie pakiety.
</p>
<p>
 Debian jest systemem operacyjnym (OS), a nie tylko jądrem (tak właściwie jest
 czymś więcej niż systemem operacyjnym, gdyż zawiera tysiące aplikacji).
 W związku z tym, podczas gdy większość adaptacji Debiana jest opartych na
 Linuksie, istnieją także adaptacje oparte na jądrach FreeBSD, NetBSD oraz
 Hurd.
</p>
<div class="important">
<p>
 Ta strona jest w trakcie rozwoju. Nie wszystkie z adaptacji
 mają własne strony, a znaczna część z istniejących znajduje się na innych
 serwerach. Wciąż pracujemy nad zebraniem informacji o wszystkich
 projektach, by móc je odzwierciedlić na stronach Debiana.
 Więcej adaptacji może być <a href="https://wiki.debian.org/CategoryPorts">opisana</a>
na wiki.
</p>
</div>

<toc-add-entry name="portlist-released">Lista oficjalnych adaptacji</toc-add-entry>
<br/>

<table class="tabular" summary="">
<tbody>

<tr>
 <th>Adaptacja</th>
 <th>Architektura</th>
 <th>Opis</th>
 <th>Stan</th>
</tr>

<tr>
 <td><a href="amd64/">amd64</a></td>
 <td>64-bitowe PC (amd64)</td>
 <td>
 Po raz pierwszy oficjalnie wydana w wersji 4.0 Debiana.
 Adaptacja dla 64-bitowych procesorów x86. Celem jest
 wsparcie zarówno 32-bitowej, jak i 64-bitowej platformy tej architektury.
 Port ten obsługuje 64-bitowe procesory Opteron, Athlon i Sempron firmy AMD
 oraz procesory Intel 64 Intela: Pentium&nbsp;D oraz serie Xeon i Core.
 </td>
 <td><a href="$(HOME)/releases/stable/amd64/release-notes/">wydana</a></td>
</tr>

<tr>
<td><a href="arm/">arm64</a></td>
<td>ARM 64-bit (AArch64)</td>
<td>Wersja 8 architektury ARM zawierająca AArch64, nowy zestaw 64-bitowych instrukcji. 
Od wydania wersji Debian 8.0 adaptacja arm64 została dołączona do Debiana w celu 
wsparcia nowego zestawu instrukcji na procesorach takich jak Applied Micro X-Gene, 
AMD Seattle oraz Cavium ThunderX.</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="arm/">armel</a></td>
 <td>EABI ARM</td>
 <td>Najstarsza obecnie adaptacja ARM Debiana wspierająca procesory ARM little-endian 
 z zestawem instrukcji v4t.</td>
 <td><a href="$(HOME)/releases/stable/armel/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="arm/">armhf</a></td>
 <td>ABI ARM ze sprzętowym FPU</td>
 <td>
 Wiele współczesnych płyt i urządzeń ARM 32-bit zawiera jednostkę zmiennoprzecinkową (FPU), 
 ale adaptacja Debiana <q>armel</q> prawie jej nie wykorzystuje.
 Adaptacja <q>armhf</q> została zapoczątkowana by poprawić tą sytuację oraz wykorzystać inne funkcje nowoczesnych procesorów ARM.
 Adaptacja Debiana <q>armhf</q> wymaga co najmniej procesora ARMv7 z Thumb-2 oraz 
 ze wsparciem operacji zmiennoprzecinkowych VFP3-D16.</td>
 <td><a href="$(HOME)/releases/stable/armhf/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="i386/">i386</a></td>
 <td>32-bitowe PC (i386)</td>
 <td>
 Pierwsza z architektur, więc właściwie pierwowzór, a nie adaptacja.
 Pierwotne wersje Linuksa zostały stworzone dla procesorów Intel 386, stąd
 nazwa skrótowa. Debian działa na wszystkich procesorach IA-32 produkowanych
 przez Intela (włączając w to wszystkie procesory Pentium i ostatnio Core Duo w
 trybie 32-bitowym), AMD (K6, wszystkie Athlony i Athlony64 w trybie
 32-bitowym), Cyrixa i innych producentów.
 </td>
 <td><a href="$(HOME)/releases/stable/i386/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="mips/">mips</a></td>
 <td>MIPS (tryb big-endian)</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 3.0.
 Debian jest adaptowany dla architektury MIPS, używane przez komputery SGI
 (MSB pierwszy = big-endian).</td>
 <td><a href="$(HOME)/releases/stable/mips/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="mipsel/">mipsel</a></td>
 <td>MIPS (tryb little-endian)</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 3.0.
 Debian jest adaptowany dla architektury MIPS, używane przez komputery
 Digital DECstation (LSB pierwszy = little-endian).</td>
 <td><a href="$(HOME)/releases/stable/mipsel/release-notes/">wydana</a></td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-bit little-endian mode)</td>
<td>
Adaptacja little-endian używająca ABI N64, ISA MIPS64r1 oraz sprzętowego 
wsparcia operacji zmiennoprzecinkowych. Część oficjalnego wydania od wersji Debian 9.
</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="powerpc/">ppc64el</a></td>
 <td>POWER7+, POWER8</td>
 <td>Pierwsze oficjalne wydanie wraz z Debian 8.0. Adaptacja ppc64 little-endian 
  używająca nowego ABI Open Power ELFv2.</td>
 <td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">wydana</a></td>
</tr>


<tr>
 <td><a href="s390x/">s390x</a></td>
 <td>System z</td>
 <td>
  Pierwsze oficjalne wydanie wraz z Debian 7.0. 64-bitowa przestrzeń użytkownika 
  dla mainframe'ów IBM <q>System z</q>.
 </td>
 <td><a href="$(HOME)/releases/stable/s390x/release-notes/">wydana</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="ports-unreleased">Lista innych adaptacji</toc-add-entry>

<div class="tip">
<p>
 Pod adresem <url "https://cdimage.debian.org/cdimage/ports"/> dostępne są 
 nieoficjalne obrazy instalacyjne dla niektórych z poniższych adaptacji. 
 Obrazy te są zarządzane przez odpowiedni Zespół Debiana ds. Adaptacji. 
</p>
</div>

<table class="tabular" summary="">
<tbody>

<tr>
 <th>Adaptacja</th>
 <th>Architektura</th>
 <th>Opis</th>
 <th>Stan</th>
</tr>

<tr>
 <td><a href="alpha/">alpha</a></td>
 <td>Alpha</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 2.1. Jedna ze starszych i
 całkiem stabilnych adaptacji.
 Nie spełniła kryteriów wejścia do wydania Debiana 6.0 <q>squeeze</q>, i w
 konsekwencji została usunięta z archiwum.</td>
 <td>zarzucona</td>
</tr>

<tr>
 <td><a href="arm/">arm</a></td>
 <td>OABI ARM</td>
 <td>
 Ta adaptacja działa na różnego rodzaju urządzeniach wbudowanych, jak routery
 lub urządzenia NAS.
 Po raz pierwszy wydana oficjalnie w Debianie 2.2, i wspierana aż do Debiana
 5.0 (włącznie), gdzie została zastąpiona adaptacją <q>armel</q>.</td>
 <td>zastąpiona przez armel</td>
</tr>

<tr>
 <td><a href="http://avr32.debian.net/">AVR32</a></td>
 <td>32-bitowy Atmel RISC</td>
 <td>
 Adaptacja na 32 bitową architekturę RISC Atmela, AVR32.</td>
 <td>zarzucona</td>
</tr>

<tr>
 <td><a href="hppa/">hppa</a></td>
 <td>HP PA-RISC</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 3.0 <q>woody</q>.
 To adaptacja dla architektury PA-RISC Hewletta-Packarda.
 Nie spełniła kryteriów wejścia do wydania Debiana 6.0 <q>squeeze</q>, i w
 konsekwencji została usunięta z archiwum.</td>
 <td>zarzucona</td>
</tr>

<tr>
 <td><a href="hurd/">hurd-i386</a></td>
 <td>32-bitowe PC (i386)</td>
 <td>
 GNU Hurd to nowy system operacyjny tworzony przez zespół GNU.
 Debian GNU/Hurd zamierza być (możliwe, że pierwszym) systemem operacyjnym GNU.
 Obecny projekt jest oparty o architekturę i386.</td>
 <td>prace w toku</td>
</tr>

<tr>
 <td><a href="ia64/">ia64</a></td>
 <td>Intel Itanium IA-64</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 3.0.
 Adaptacja dla pierwszej 64 bitowej architektury Intela.
 Uwaga: nie należy jej mylić z ostatnimi rozszerzeniami
 64-bitowymi dla procesorów Pentium 4 i Celeronów, zwanymi Intel 64. Dla tych
 procesorów sprawdź adaptację AMD64.</td>
 <td><a href="$(HOME)/releases/stable/ia64/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
 <td>64-bitowe PC (amd64)</td>
 <td>
 Po raz pierwszy oficjalnie wydana w wersji 6.0 Debiana jako <q>pokaz
 przedpremierowy</q> i pierwsza adaptacja dla jądra innego niż Linux wydana
 przez Debiana.
 Jest to adaptacja systemu Debian GNU dla jądra FreeBSD.</td>
 <td><a href="$(HOME)/releases/stable/kfreebsd-amd64/release-notes/">wydana</a></td>
</tr>

<tr>
 <td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
 <td>32-bitowe PC (i386)</td>
 <td>
 Po raz pierwszy oficjalnie wydana w wersji 6.0 Debiana jako <q>pokaz
 przedpremierowy</q> i pierwsza adaptacja dla jądra innego niż Linux wydana
 przez Debiana.
 Jest to adaptacja systemu Debian GNU dla jądra FreeBSD. Od wydania Debian 8.0 
 nie jest częścią oficjalnego wydania.</td>
 <td><a href="$(HOME)/releases/stable/kfreebsd-amd32/release-notes/">prace w toku</a></td>
</tr>

<tr>
 <td><a href="http://www.linux-m32r.org/">m32</a></td>
 <td>M32R</td>
 <td>
 Adaptacja dla 32-bitowych mikroprocesorów Renesas Technology.</td>
 <td>martwa</td>
</tr>


<tr>
 <td><a href="m68k/">m68k</a></td>
 <td>Motorola 68k</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 2.0.
 Ta adaptacja nie spełniła kryteriów wejścia do wydania Debiana 4.0,
 dlatego nie została ona włączona do Etch ani późniejszych wydań i została
 przeniesiona do archiwum debian-ports.
 Adaptacja Debiana dla m68k działa na szerokiej gamie
 komputerów opartych o procesory z rodziny Motorola 68k &mdash;
 w szczególności stacjach roboczych Sun3 oraz komputerach osobistych Apple
 Macintosh, Atari i Amiga.
 </td>
 <td>prace w toku</td>
</tr>

<tr>
 <td><a href="netbsd/">netbsd-i386</a></td>
 <td>32-bitowe PC (i386)</td>
 <td>
 To adaptacja całego sytemu operacyjnego Debian (włącznie z apt, dpkg i
 programami użytkowymi GNU) dla jądra NetBSD.
 Adaptacja ta została zarzucona zanim doczekała się wydania.
 </td>
 <td>martwa</td>
</tr>

<tr>
 <td><a href="netbsd/alpha/">netbsd-alpha</a></td>
 <td>Alpha</td>
 <td>
 To adaptacja całego sytemu operacyjnego Debian (włącznie z apt, dpkg i
 programami użytkowymi GNU) dla jądra NetBSD.
 Adaptacja ta została zarzucona zanim doczekała się wydania.
 </td>
 <td>martwa</td>
</tr>
<tr>
<td><a href="http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Adaptacja dla procesora 
<a href="https://openrisc.io/">OpenRISC</a> 1200 rozwijanego 
jako procesor open source.</td>
<td>martwa</td>
</tr>

<tr>
 <td><a href="powerpc/">powerpc</a></td>
 <td>Motorola/IBM PowerPC</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 2.2. Działa na wielu z
 modeli Apple Macintosh PowerMac oraz na komputerach o architekturach CHRP i
 PReP. Od wersji Debian 9 nie jest częścią oficjalnego wydania.
 <td><a href="$(HOME)/releases/stable/powerpc/release-notes/">zarzucona</a></td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>Adaptacja na sprzęt "Signal Processing Engine" obecny na w energooszczędnych 
procesorach 32-bitowych FreeScale oraz IBM <q>e500</q>.</td>
<td>prace w toku</td>
</tr>

<tr>
 <td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
 <td>RISC-V (64-bit little endian)</td>
 <td>Adaptacja dla procesora <a href="http://riscv.org/">RISC-V</a>, 
 wolnego/otwartego ISA, w szczególności w wersji 64-bit little-endian.</td>
 <td>prace w toku</td>
</tr>

<tr>
 <td><a href="s390/">s390</a></td>
 <td>S/390 i zSeries</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 3.0.
 Adaptacja Debiana dla serwerów IBM S/390. Zastąpiona 
 przez adaptację s390x wraz z Debian 8.</td>
 <td>zastąpiona przez s390x</td>
</tr>

<tr>
 <td><a href="sparc/">sparc</a></td>
 <td>Sun SPARC</td>
 <td>
 Po raz pierwszy wydana oficjalnie w Debianie 2.1. Działa na komputerach z
 serii stacji roboczych Sun UltraSPARC i części ich następców z rodziny
 sun4. Od wydania Debian 8 Sparc nie jest już wydawaną architekturą ze względu 
 na niewystarczające wsparcie deweloperów. Ma być wkrótce zastąpiona przez 
 Sparc64.</td>
 <td>będzie zastąpiona przez sparc64</td>
</tr>

<tr>
 <td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
 <td>SPARC 64-bit</td>
 <td>64-bitowa adaptacja na procesory SPARC.</td>
 <td>prace w toku</td>
</tr>

<tr>
 <td><a href="https://wiki.debian.org/SH4">sh4</a></td>
 <td>SuperH</td>
 <td>Adaptacja na procesory Hitachi SuperH. Wspiera również 
 otwartoźródłowy procesor <a href="http://j-core.org/">J-Core</a>.</td>
 <td>prace w toku</td>
</tr>

<tr>
 <td><a href="https://wiki.debian.org/X32Port">x32</a></td>
 <td>64-bitowy PC z 32-bitowymi wskaźnikami</td>
 <td>X32 jest to ABI dla procesorów amd64/x86 używających 32-bitowych 
 wskaźników. Ideą jest połączenie dużego zestawu rejestrów x86_64 
 z mniejszą pamięcią i cache footprint będącą wynikiem używania 
 32-bitowych wskaźników.</td>
 <td>prace w toku</td>
</tr>
</tbody>
</table>

<div class="note">
<p>Wiele z powyższych nazw komputerów i procesorów
jest zarejestrowanymi znakami towarowymi ich wytwórców.</p>
</div>
